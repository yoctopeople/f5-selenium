WINDOWS Install:

Download: Python 3
- in the install options specify pip
- in admin CMD shell: pip install selenium

Edit scriptwin.py to point to the absolute path of the webdriver: C:\.....\chromedriver.exe

Run python script for windows

    python scriptwin.py



MAC Install:

    brew install python
    sudo easy_install pip

Download webdriver for your chrome browser and put into drivers folder. then fix path in the script

    https://chromedriver.chromium.org/downloads

Install packages:
 
    sudo pip install selenium
    
Run python script

    python script.py


