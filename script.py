import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import selenium.webdriver.chrome.service as service

def get_clear_browsing_button(driver):
    return driver.find_element_by_css_selector('* /deep/ #clearBrowsingDataConfirm')

service = service.Service('./drivers/chromedriver_mac_78')
service.start()
capabilities = {'chrome.binary': './drivers/chromedriver_mac_78'}
driver = webdriver.Remote(service.service_url, capabilities)

# open default page
driver.get('http://eu-auction.cloudservicesdemo.net/')
time.sleep(3)
try:
    # click sign in
    driver.find_element_by_xpath('//*[@id="root"]/section/div[2]/div/nav/button[1]').click()
    time.sleep(1)
    # type username and password
    username = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div[2]/div/form/div[1]/input[1]')
    username.click()
    username.send_keys('Craig')
    time.sleep(1)
    password = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div[2]/div/form/div[1]/input[2]')
    password.click()
    password.send_keys('123')
    time.sleep(1)
    # click sign in
    driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div[2]/div/form/div[2]/button').click()
    time.sleep(3)
except:
    print('blocked');

driver.get('http://eu-auction.cloudservicesdemo.net/lots')
# pagination
for x in range(5):
    driver.get('http://eu-auction.cloudservicesdemo.net/lot/' + str(x+1015))
    time.sleep(3)
    driver.execute_script("window.history.go(-1)")
    time.sleep(1)
    driver.refresh()
    time.sleep(3)


driver.get('http://eu-auction.cloudservicesdemo.net/lots')
time.sleep(5)

driver.quit()
